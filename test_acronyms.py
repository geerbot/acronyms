import pytest, unittest, os

class testAdding(unittest.TestCase):
    @pytest.fixture(scope="class")
    def init(self):
        os.remove('.data')
    
    def test1AddFirstAcronym(self):
        os.system('python acronymble.py -a TEST="Test1"')
        res = os.popen('python acronymble.py -f TEST').read().strip('\n')
        assert res == 'TEST=Test1', "Invalid retrieve {}".format(res)

    def test2AddSimilarButLongerAcronym(self):
        os.system('python acronymble.py -a TEST1="Another"')
        res = os.popen('python acronymble.py -f TEST1').read().strip('\n')
        assert res == 'TEST1=Another', "Invalid retrieve {}".format(res)
    
    def test3AddSimilarButShorterAcronym(self):
        os.system('python acronymble.py -a TES="Tes"')
        res = os.popen('python acronymble.py -f TES').read().strip('\n')
        assert res == 'TES=Tes', "Invalid retrieve {}".format(res)
