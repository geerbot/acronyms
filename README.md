# Recommended Install
## Linux
* Place acronyms.py script somewhere such as ~/.scripts/
* Add line "alias acronyms="~/.scripts/acronyms.py"

# Usage
## Add acronym
acronyms -a THOR="The hammers over rated"

## Find acronym
acronyms -f THOR
