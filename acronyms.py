import sys
import optparse
import os

def usage():
    print('Invalid option')
    print('Usage: main.py [option] [value(s)]\n\n \
            OPTIONS:\n\t-h\t--help \
            \n\t-a\t--add (acronym to add)=(meaning of acronym) \
            \n\t-r\t--remove (acronym to remove) \
            \n\t-f\t--find (acronym to find)')

def validateAddAcronym(opts):
    if opts.a != "":
        vals = opts.a.split('=')
        if len(vals) < 2 or len(vals) > 2:
            usage()
            exit(1)

def definitionAlreadyExists(line, acro):
    fields=line.split('=')[1].split(',')
    for field in fields:
        if field == acro:
            return True
    return False

def addAcronym(val):
    exists=False
    if os.path.exists('.data'):
        tmp = open('.tmp','w')
        with open('.data', 'r+') as fileData:
            for lineData in fileData:
                if lineData.split('=')[0] == val.split('=')[0]:
                    exists=True
                    if not definitionAlreadyExists(lineData, val.split('=')[0]):
                        tmp.write(newLineData)
                    else:
                        newLineData = lineData.replace('\n', ',' + val.split('=')[1] + '\n')
                        tmp.write(newLineData)
                        print("Added {}".format(newLineData))
                else:
                    tmp.write(lineData)
            if not exists:
                tmp.write(val + '\n')
                print("Added {}".format(val))
        os.rename('.tmp','.data')
    else:
        with open('.data', 'w') as fileData:
            print('writing {}'.format(val))
            fileData.write(val + '\n')

def findAcronym(val):
    with open('.data','r+') as f:
        for l in f:
            if l.split('=')[0] == val:
                print(l)

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option('-a', action="store", default="")
    parser.add_option('-r', action="store", default="")
    parser.add_option('-f', action="store", default="")
#   parser.add_option('-h', action="callback", callback=usage)

    opts, args = parser.parse_args()
    if len(opts.a) > 0:
        validateAddAcronym(opts)
        addAcronym(opts.a)
    if len(opts.f) > 0:
        findAcronym(opts.f)
